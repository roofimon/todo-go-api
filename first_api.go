package main

import (
	"log"
	"net/http"

	"fmt"

	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	_ "github.com/mattn/go-sqlite3"
)

var db *gorm.DB

func init() {
	var err error
	db, err = gorm.Open("sqlite3", "todos.db")
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	e := echo.New()
	e.Use(middleware.CORS())

	db.AutoMigrate(todo{})

	e.GET("/", helloWorld)
	e.GET("/todo", todos)
	e.POST("/todo", save)

	e.Logger.Fatal(e.Start(":1323"))
}

type todo struct {
	ID    string `json:"id"`
	Topic string `json:"topic"`
}

func save(c echo.Context) error {
	var nt todo
	err := c.Bind(&nt)
	if err != nil {
		log.Fatal(err)
	}
	db = db.Create(&nt)
	if db.Error != nil {
		return c.JSON(http.StatusBadRequest, todo{})
	}
	return c.JSON(http.StatusOK, nt)
}

func todos(c echo.Context) error {
	fmt.Println("------------------ TODOS ----------------------")
	var all []todo
	db = db.Find(&all)
	if db.Error != nil {
		return c.JSON(http.StatusBadRequest, todo{})
	}
	return c.JSON(http.StatusOK, all)
}

func helloWorld(c echo.Context) error {
	return c.String(http.StatusOK, "Hello, World!")
}
